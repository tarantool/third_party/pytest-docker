# -*- coding: utf-8 -*-


import mock
import pytest
import subprocess

from pytest_docker import (
    DockerComposeExecutor,
    DockerExecutor,
    docker_services,
    Services,
)

@pytest.fixture(scope='session')
def docker_inspect_output(pytestconfig):
    return str(pytestconfig.rootdir.join('tests/docker_inspect_output.txt'))

def test_docker_services(docker_inspect_output):
    """Automatic teardown of all services."""

    with mock.patch('subprocess.check_output') as check_output:
        check_output.side_effect = [b'', b"b7069cc8b9e24a638ba51b524ff3e820" + \
                                    b"457028d094fc241e6c6fee455f223fc0\n",
                                    open(docker_inspect_output).read(),
                                    b'']
        check_output.returncode = 0

        assert check_output.call_count == 0

        # The fixture is a context-manager.
        gen = docker_services(
            'docker-compose.yml',
            docker_compose_project_name='pytest123',
            docker_allow_fallback=False,
        )
        services = next(gen)
        assert isinstance(services, Services)

        assert check_output.call_count == 3

        # Can request port for services.
        port = services.port_for('hello', 80)
        assert port == 32770

        # Next yield is last.
        with pytest.raises(StopIteration):
            print(next(gen))

        assert check_output.call_count == 5

    # Both should have been called.
    assert check_output.call_args_list == [
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'up --build -d',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'ps -q',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker inspect '
            'b7069cc8b9e24a638ba51b524ff3e820457028d094fc241e6c6fee455f223fc0',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" down -v',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'ps -q',
            shell=True, stderr=subprocess.STDOUT,
        )
    ]


def test_docker_services_unused_port(docker_inspect_output):
    """Complain loudly when the requested port is not used by the service."""

    with mock.patch('subprocess.check_output') as check_output:
        check_output.side_effect = [b'', b"b7069cc8b9e24a638ba51b524ff3e820" + \
                                    b"457028d094fc241e6c6fee455f223fc0\n",
                                    open(docker_inspect_output).read(),
                                    b'']
        check_output.returncode = 0

        assert check_output.call_count == 0

        # The fixture is a context-manager.
        gen = docker_services(
            'docker-compose.yml',
            docker_compose_project_name='pytest123',
            docker_allow_fallback=False,
        )
        services = next(gen)
        assert isinstance(services, Services)

        assert check_output.call_count == 3

        # Can request port for services.
        with pytest.raises(ValueError) as exc:
            print(services.port_for('hello', 123))
        assert str(exc.value) == (
            "Service %s doesn't expose port: %s" % ('hello', 123)
        )

        # Next yield is last.
        with pytest.raises(StopIteration):
            print(next(gen))

        assert check_output.call_count == 5

    # Both should have been called.
    assert check_output.call_args_list == [
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'up --build -d',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'ps -q',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker inspect '
            'b7069cc8b9e24a638ba51b524ff3e820457028d094fc241e6c6fee455f223fc0',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" down -v',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'ps -q',
            shell=True, stderr=subprocess.STDOUT,
        ),
    ]


def test_docker_services_failure():
    """Propagate failure to start service."""

    with mock.patch('subprocess.check_output') as check_output:
        check_output.side_effect = [
            subprocess.CalledProcessError(
                1, 'the command', b'the output',
            ),
        ]
        check_output.returncode = 1

        # The fixture is a context-manager.
        gen = docker_services(
            'docker-compose.yml',
            docker_compose_project_name='pytest123',
            docker_allow_fallback=False,
        )

        assert check_output.call_count == 0

        # Failure propagates with improved diagnoatics.
        with pytest.raises(Exception) as exc:
            print(next(gen))
        assert str(exc.value) == (
            'Command %r returned %d: """%s""".' % (
                "the command", 1, 'the output',
            )
        )

        assert check_output.call_count == 1

    # Tear down code should not be called.
    assert check_output.call_args_list == [
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'up --build -d',
            shell=True, stderr=subprocess.STDOUT,
        ),
    ]


def test_wait_until_responsive_timeout():
    clock = mock.MagicMock()
    clock.side_effect = [0.0, 1.0, 2.0, 3.0]

    with mock.patch('time.sleep') as sleep:
        docker_compose = DockerComposeExecutor(
            compose_files='docker-compose.yml',
            compose_project_name="pytest123")
        docker = DockerExecutor()
        services = Services(docker_compose, docker)
        with pytest.raises(Exception) as exc:
            print(services.wait_until_responsive(
                check=lambda: False,
                timeout=3.0,
                pause=1.0,
                clock=clock,
            ))
        assert sleep.call_args_list == [
            mock.call(1.0),
            mock.call(1.0),
            mock.call(1.0),
        ]
    assert str(exc.value) == (
        'Timeout reached while waiting on service!'
    )


def test_get_port_docker_allow_fallback_docker_online(docker_inspect_output):
    with mock.patch('subprocess.check_output') as check_output:
        check_output.side_effect = [b'', b'', b'b7069cc8b9e24a638ba51b524ff' + \
                                    b'3e820457028d094fc241e6c6fee455f223fc0\n',
                                    open(docker_inspect_output).read(),
                                    b'']
        check_output.returncode = 0

        assert check_output.call_count == 0

        # The fixture is a context-manager.
        gen = docker_services(
            'docker-compose.yml',
            docker_compose_project_name='pytest123',
            docker_allow_fallback=True,
        )
        services = next(gen)
        assert isinstance(services, Services)

        assert check_output.call_count == 4

        # Can request port for services.
        port = services.port_for('hello', 80)
        assert port == 32770

        # Next yield is last.
        with pytest.raises(StopIteration):
            print(next(gen))

        assert check_output.call_count == 6

    assert check_output.call_args_list == [
        mock.call(
            'docker ps',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'up --build -d',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'ps -q',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker inspect '
            'b7069cc8b9e24a638ba51b524ff3e820457028d094fc241e6c6fee455f223fc0',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" down -v',
            shell=True, stderr=subprocess.STDOUT,
        ),
        mock.call(
            'docker-compose -f "docker-compose.yml" -p "pytest123" '
            'ps -q',
            shell=True, stderr=subprocess.STDOUT,
        ),
    ]


def test_get_port_docker_allow_fallback_docker_offline():
    with mock.patch('subprocess.check_output') as check_output:
        check_output.side_effect = [
            subprocess.CalledProcessError(
                1, 'the command', b'the output',
            ),
        ]
        check_output.returncode = 1

        gen = docker_services(
            'docker-compose.yml',
            docker_compose_project_name='pytest123',
            docker_allow_fallback=True,
        )
        services = next(gen)
        assert services.port_for('abc', 123) == 123

        # Next yield is last.
        with pytest.raises(StopIteration):
            print(next(gen))
