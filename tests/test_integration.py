# -*- coding: utf-8 -*-


import requests

from requests.exceptions import (
    ConnectionError,
)


def is_responsive(url):
    """Check if something responds to ``url``."""
    try:
        response = requests.get(url)
        if response.status_code == 200:
            return True
    except ConnectionError:
        return False


def test_integration(docker_ip, docker_services):
    """Showcase the power of our Docker fixtures!"""

    # Build URL to service listening on random port.
    url = 'http://%s:%d/' % (
        docker_ip,
        docker_services.port_for('hello', 80),
    )

    # Wait until service is responsive.
    docker_services.wait_until_responsive(
        check=lambda: is_responsive(url),
        timeout=30.0,
        pause=0.1,
    )

    assert "hello" in docker_services.services()
    docker_services.stop("hello")
    assert docker_services.services()["hello"]["Running"] is False
    docker_services.start("hello")
    assert docker_services.services()["hello"]["Running"] is True
    docker_services.down()
    assert "hello" not in docker_services.services()
    docker_services.up("hello")
    assert docker_services.services()["hello"]["Running"] is True
    assert docker_services.execute("hello", "echo '1'").strip() == '1'

    url = 'http://%s:%d/' % (
        docker_ip,
        docker_services.port_for('hello', 80),
    )

    # Wait until service is responsive.
    docker_services.wait_until_responsive(
        check=lambda: is_responsive(url),
        timeout=30.0,
        pause=0.1,
    )

    # Contact the service.
    response = requests.get(url)
    response.raise_for_status()
    print(response.text)
