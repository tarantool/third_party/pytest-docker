# -*- coding: utf-8 -*-


import attr
import os
import pytest
import re
import subprocess
import time
import timeit
import simplejson as json


def execute(command, success_codes=(0,)):
    """Run a shell command."""
    try:
        output = subprocess.check_output(
            command, stderr=subprocess.STDOUT, shell=True,
        )
        status = 0
    except subprocess.CalledProcessError as error:
        output = error.output or b''
        status = error.returncode
        command = error.cmd
    output = output.decode('utf-8')
    if status not in success_codes:
        raise Exception(
            'Command %r returned %d: """%s""".' % (command, status, output)
        )
    return output


@pytest.fixture(scope='session')
def docker_ip():
    """Determine IP address for TCP connections to Docker containers."""

    # When talking to the Docker daemon via a UNIX socket, route all TCP
    # traffic to docker containers via the TCP loopback interface.
    docker_host = os.environ.get('DOCKER_HOST', '').strip()
    if not docker_host:
        return '127.0.0.1'

    match = re.match('^tcp://(.+?):\d+$', docker_host)
    if not match:
        raise ValueError(
            'Invalid value for DOCKER_HOST: "%s".' % (docker_host,)
        )
    return match.group(1)


@attr.s()
class Services(object):
    """."""

    _docker_compose = attr.ib()
    _docker = attr.ib()
    _docker_allow_fallback = attr.ib(default=False)

    _services = attr.ib(init=False, default=attr.Factory(dict))

    def up(self, service=None, recreate=True):
        """Up a specified container or all containers otherwise"""
        # Up all containers
        command = "up -d"
        if recreate:
            command += " --force-recreate"
        if service:
            command += " %s" % (service)
        self._docker_compose.execute(command)
        self.refresh_status()

    def down(self):
        """Down a specified container or all containers otherwise"""
        # Down all containers
        self._docker_compose.execute("down -v")
        self.refresh_status()

    def refresh_status(self):
        """Fill _services attribute with containers status"""
        containers_id = self._docker_compose.execute("ps -q")
        self._services = {}
        if containers_id == "":
            return
        for id in containers_id.strip().split("\n"):
            info_string = self._docker.execute("inspect %s" % (id))
            info = json.loads(info_string)[0]
            if 'Name' not in info:
                continue
            service = info["Name"].split('_')[1]
            self._services.setdefault(service, {})
            self._services[service]['id'] = info['Id']
            self._services[service]['name'] = info['Name']
            self._services[service]['Status'] = info['State']['Status']
            self._services[service]['Running'] = info['State']['Running']
            self._services[service]['Ports'] = info['NetworkSettings']['Ports']

    def execute(self, service, command):
        """Execute specified command into service container"""

        return self._docker_compose.execute("exec -T %s %s" % (service, command))

    def port_for(self, service, port):
        """Get the effective bind port for a service."""

        # Return the container port if we run in no Docker mode.
        if self._docker_allow_fallback:
            return port

        # Lookup in the status.
        ports = self._services.get(service, {}).get('Ports', None)
        if ports is not None:
            for name, p in ports.iteritems():
                if port == int(name.split('/')[0]):
                    return int(p[0]['HostPort'])

        raise ValueError(
            "Service %s doesn't expose port: %s" % (service, port)
        )

    def is_running(self, service):
        """Return true if container with service is running.
           Return false otherwise"""
        if service in self._services:
            return self._services[service]['Running']
        return False

    def services(self):
        """Return a list of services"""
        return self._services

    def stop(self, service):
        """Stop a container with the specified service"""
        if self.is_running(service):
            self._docker_compose.execute('stop %s' % (service))
            self.refresh_status()

    def start(self, service):
        """Start a container with the specified service"""
        if not self.is_running(service):
            self._docker_compose.execute('start %s' % (service))
            self.refresh_status()

    def wait_until_responsive(self, check, timeout, pause,
                              clock=timeit.default_timer):
        """Wait until a service is responsive."""

        ref = clock()
        now = ref
        while (now - ref) < timeout:
            if check():
                return
            time.sleep(pause)
            now = clock()

        raise Exception(
            'Timeout reached while waiting on service!'
        )


def str_to_list(arg):
    if isinstance(arg, (list, tuple)):
        return arg
    return [arg]


@attr.s(frozen=True)
class DockerComposeExecutor(object):
    _compose_files = attr.ib(convert=str_to_list)
    _compose_project_name = attr.ib()

    def execute(self, subcommand):
        command = "docker-compose"
        for compose_file in self._compose_files:
            command += ' -f "{}"'.format(compose_file)
        command += ' -p "{}" {}'.format(self._compose_project_name, subcommand)
        return execute(command)


@attr.s(frozen=True)
class DockerExecutor(object):
    def execute(self, subcommand):
        command = "docker"
        command += ' {}'.format(subcommand)
        return execute(command)


@pytest.fixture(scope='session')
def docker_compose_file(pytestconfig):
    """Get the docker-compose.yml absolute path.

    Override this fixture in your tests if you need a custom location.

    """
    return os.path.join(
        str(pytestconfig.rootdir),
        'tests',
        'docker-compose.yml'
    )


@pytest.fixture(scope='session')
def docker_compose_project_name():
    """ Generate a project name using the current process' PID.

    Override this fixture in your tests if you need a particular project name.
    """
    return "pytest{}".format(os.getpid())


@pytest.fixture(scope='session')
def docker_allow_fallback():
    """Return if want to run against localhost when docker is not available.

    Override this fixture to return `True` if you want the ability to
    run without docker.

    """
    return False


@pytest.fixture(scope='session')
def docker_services(
    docker_compose_file, docker_allow_fallback, docker_compose_project_name
):
    """Ensure all Docker-based services are up and running."""

    docker_compose = DockerComposeExecutor(
        docker_compose_file, docker_compose_project_name
    )

    docker = DockerExecutor()

    # If we allowed to run without Docker, check it's presence
    if docker_allow_fallback is True:
        try:
            execute('docker ps')
        except Exception:
            # Run against localhost
            yield Services(docker_compose, docker, docker_allow_fallback=True)
            return

    services = Services(docker_compose, docker)
    # Spawn containers.
    services.up()

    # Let test(s) run.
    yield services

    # Clean up.
    services.down()


__all__ = (
    'docker_compose_file',
    'docker_ip',
    'docker_services',
)
